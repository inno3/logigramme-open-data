# logigramme-opendata

Logigramme Open Data

Vous pouvez avoir [un aperçu du site](https://inno3.frama.io/logigramme-open-data/).

# Crédits

Ce site est basé sur les composant Open Source suivants.

- [Bootstrap](https://getbootstrap.com), sous [licence MIT](https://getbootstrap.com/docs/4.1/about/license/)
- [Popper.js](https://popper.js.org/) sous [licence MIT](https://github.com/FezVrasta/popper.js/blob/master/LICENSE.md)
- [JQuery](https://jquery.com/), sous [licence MIT](https://github.com/jquery/jquery/blob/master/LICENSE.txt)
- [jQuery decisionTree](https://www.jqueryscript.net/chart-graph/Accessible-jQuery-Decision-Tree-Flowchart-Plugin-decisionTree.html), lui aussi sous licence MIT

Le reste du code est (c) Inno³ et placé sous licence MIT également.
